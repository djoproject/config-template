#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# XXX this script is meant to be executed, not sourced.

### GLOBALS ##################################################################
CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

### INTERNAL FUNCTION ########################################################

function _bash_enable {
    config_file_name=$1

    # line to add
    part_to_add="test -f $CURRENT_PATH/bashrc && source $CURRENT_PATH/bashrc"

    # check if line already exist
    part_line_number=$(grep -F -n "$part_to_add" "$HOME/$config_file_name" | head -n 1 | cut -d: -f1)

    # need to install ?
    if [[ -z "$part_line_number" ]]; then
        echo "$part_to_add" >> "$HOME/$config_file_name"
    fi
}

function show_help {
cat << EOF
usage: ${0##*/} [options]
Options:
  -b            Enable bash support
  -h            Show this help message
  -f            Force install when possible
  -i            ignore skip
EOF
}

function _describe {
    if [[ $# -lt 3 ]]; then
        echo "ERROR: _describe not enough arguments, need 3, got $#" >&2
        return 1
    fi

    local target_file
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        target_file="$CUSTOM_CONFIG_DIRECTORY/documentation/$1_description"

        # create documentation directory if needed
        local target_directory
        target_directory="$CUSTOM_CONFIG_DIRECTORY/documentation"
        if [[ ! ( -d "$target_directory" ) ]]; then
            if ! mkdir -p "$target_directory"; then
                echo "ERROR: fail to create directory $target_directory." >&2
                return 1
            fi
        fi
    else
        target_file="$HOME/.$1_description"
    fi

    # remove any existing descrition
    if [[ -f "$target_file" ]]; then
        sed -i "/^$2 /d" "$target_file"
    fi

    # register documentation
    echo "$2 $3" >> "$target_file"
}

### EXPORTED FUNCTIONS #######################################################

# define install function
function install_file {
    cp "$1" "$2"
}


function install_bin {
    local target_directory
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        target_directory="$CUSTOM_CONFIG_DIRECTORY"
    else
        target_directory="$HOME"
    fi

    # create bin directory if needed
    target_directory="$target_directory/bin/"
    if [[ ! ( -d "$target_directory" ) ]]; then
        if ! mkdir -p "$target_directory"; then
            local binary_name
            if [[ -z "$2" ]]; then
                binary_name="$1"
            else
                binary_name="$2 ($1)"
            fi
            echo "ERROR: fail to create directory $target_directory, binary $binary_name won't be installed." >&2
            return 1
        fi
    fi

    # register script
    if [[ -z "$2" ]]; then
        cp "$1" "$target_directory"
    else
        cp "$1" "$target_directory/$2"
    fi

    # register documentation even it is empty.  This way, we get a list of scripts
    if [[ -z "$2" ]]; then
        _describe "script" "$1" "$3"
    else
        _describe "script" "$2" "$3"
    fi
}

function describe_function {
    _describe "function" "$@"
}

function describe_alias {
    _describe "alias" "$@"
}

function describe_variable {
    _describe "variable" "$@"
}

function alias_related_to {
    _describe "alias_to" "$@"
}

### MAIN #####################################################################

OPTIND=1
enable_bash_support=0
force=0
skip=1

while getopts ":hbfi" opt; do
    case $opt in
        h)
            show_help
            exit 0
            ;;
        b)  enable_bash_support=1
            ;;
        f)  force=1
            ;;
        i)  skip=0
            ;;
        ?)
            echo "unknown option -- ${1:1:1}"
            show_help >&2
            exit 1
            ;;
        *)
            echo "unknown option -- $1"
            show_help >&2
            exit 1
            ;;
    esac
done

shift "$((OPTIND-1))"   # Discard the options and sentinel --

# install each component
for directory in "$CURRENT_PATH/"*/ ; do
    # if no result, continue.
    if [[ "$directory" = "$CURRENT_PATH/*/" ]]; then
        continue
    fi

    # remove last slash
    directory="${directory::-1}"

    echo "########################"
    base_directory=$(basename "$directory")
    echo "Processing $base_directory..."
    cd "$directory" || { echo "ERROR: skip this component" >&2 ;echo "########################"; continue; }

    if [[ -e "skip" ]]; then
        echo "INFO: disabled component. skip it."
        continue
    fi

    if [[ $skip -eq 1 ]] && [[ -f "install" ]]; then
        if [[ $force -eq 1 ]]; then
            # shellcheck source=/dev/null
            source ./install -f
        else
            # shellcheck source=/dev/null
            source ./install
        fi

        instal_ret=$?
        if [[ $instal_ret -ne 0 ]]; then
            echo "WARNING: last install has encountered issue(s)"

            if [[ $force -eq 0 ]]; then
                echo "<!> abort installation <!>"
                exit $instal_ret
            fi
        fi
    fi

    cd  "$CURRENT_PATH" || { echo "abort installation";break;}
    echo "$base_directory processed"
    echo "########################"
    echo
done

if [[ $enable_bash_support -eq 1 ]]; then
    if [[ -f "$HOME/.bash_profile" ]]; then
        CONFIG_FILE_NAME=.bash_profile
    elif [[ -f "$HOME/.bashrc" ]]; then
        CONFIG_FILE_NAME=.bashrc
    else
        echo "ERROR: no configuration file found" >&2
        exit 1
    fi

    _bash_enable "$CONFIG_FILE_NAME"
fi
