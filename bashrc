#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# this script is meant to be sourced. not executed.

function main {
    local current_path
    current_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

    # sourcing each bashrc file insides directories
    local directory
    for directory in "$current_path/"*/ ; do
        if [[ -e "${directory}skip" ]]; then
            continue
        fi

        # sourcing cleaning file
        if [[ -f "${directory}.bash_cleaning" ]]; then
            # shellcheck source=/dev/null
            source "${directory}.bash_cleaning"
        fi

        # sourcing variables file
        if [[ -f "${directory}.bash_variables" ]]; then
            # shellcheck source=/dev/null
            source "${directory}.bash_variables"
        fi

        # sourcing aliases file
        if [[ -f "${directory}.bash_aliases" ]]; then
            # shellcheck source=/dev/null
            source "${directory}.bash_aliases"
        fi

        # sourcing functions file
        if [[ -f "${directory}.bash_functions" ]]; then
            # shellcheck source=/dev/null
            source "${directory}.bash_functions"
        fi

    done
}

main
unset main
